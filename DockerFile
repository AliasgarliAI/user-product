FROM openjdk:17-oracle
COPY build/libs/api-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/api-0.0.1-SNAPSHOT.jar"]
