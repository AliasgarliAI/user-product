package com.company.api.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
//@SecuritySchemes({
//        @SecurityScheme(name = "basicAuth", type = SecuritySchemeType.HTTP, scheme = "basic"),
//        @SecurityScheme(name = "bearerToken", type = SecuritySchemeType.HTTP, scheme = "bearer", bearerFormat = "JWT")
//})
public class SwaggerConfig {

    @Bean
    public OpenAPI customOpenApi() {
        return new OpenAPI().info(
                new Info().title("Simple Shopping Api")
                        .version("1.0")
                        .description("Demo api for shopping services")
                        .license(new License()
                                .name("Alakbar Aliasgarli's API Licence")
                                .url("https://aliasgarli.com"))
                        .contact(new Contact()
                                .name("Contact Information: \n")
                                .url("https://gitlab.com/AliasgarliAI")
                                .email("eelesgerli98@gmail.com"))

        );
    }
}
