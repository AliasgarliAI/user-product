package com.company.api.constants;

public final class ApplicationMessage {

    public static final String EMPTY_MESSAGE ="";
    public static final String DELETION_MESSAGE = "Object is deleted safely";
    public static final String ACTIVATION_MESSAGE = "Object is activated successfully";
    public static final String DEACTIVATION_MESSAGE = "Object is deactivated successfully";
    public static final String OPERATION_PERFORMED = "Operation performed successfully";

    private ApplicationMessage(){}

}
