package com.company.api.constants;

public final class ExceptionMessage {

    private ExceptionMessage() {}

    public static final String USER_NOT_FOUND = "There is not such kind of existing user";
    public static final String PRODUCT_NOT_FOUND = "There is not such kind of existing product";
    public static final String INSUFFICIENT_BALANCE = "User don't have enough balance";

    public static final String OPERATION_NOT_ACCEPTED = "Operation was not accepted";

}
