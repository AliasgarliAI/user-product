package com.company.api.controller;

import com.company.api.dto.CommonResponse;
import com.company.api.dto.ProductDto;
import com.company.api.exception.CommonNotFoundException;
import com.company.api.service.inter.ProductService;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.company.api.constants.ApplicationMessage.EMPTY_MESSAGE;

@RestController
@RequestMapping("products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @GetMapping
    public CommonResponse<List<ProductDto>> getAllProducts(
            @RequestParam(value = "pageNumber", required = false) Integer pageNumber,
            @RequestParam(value = "size", required = false) Integer size) {

        List<ProductDto> allProducts = productService.getAllProducts(pageNumber, size);

        return CommonResponse.success(allProducts, HttpStatus.OK, EMPTY_MESSAGE);
    }

    @GetMapping("/onstock")
    public CommonResponse<List<ProductDto>> getAllExistingProducts(
            @RequestParam(value = "pageNumber", required = false) Integer pageNumber,
            @RequestParam(value = "size", required = false) Integer size) {

        List<ProductDto> allExistingProducts = productService.getAllExistingProducts();
        return CommonResponse.success(allExistingProducts, HttpStatus.OK, EMPTY_MESSAGE);
    }

    @GetMapping("/product/{productId}")
    public CommonResponse<ProductDto> getProductById(@PathVariable Long productId) throws CommonNotFoundException {
        ProductDto productById = productService.getProductById(productId);
        return CommonResponse.success(productById, HttpStatus.OK, EMPTY_MESSAGE);
    }

    @GetMapping("/product")
    public CommonResponse<List<ProductDto>> getProductsByProductName(
            @RequestParam("productName") String productName) throws CommonNotFoundException {
        List<ProductDto> products = productService.getProductByName(productName);
        return CommonResponse.success(products, HttpStatus.OK, EMPTY_MESSAGE);
    }

    @PostMapping("/product")
    public CommonResponse<ProductDto> createNewProduct(
            @RequestHeader("Idempotency-Key")
            @NotBlank(message = "Idempotency-Key must not be blank")
            @Size(min = 5, max = 10, message = "Idempotency-Key length must be between 5 and 10 characters")
            String idempotencyKey,
            @RequestBody ProductDto productDto) {

        ProductDto product = productService.createProduct(idempotencyKey, productDto);

        return CommonResponse.success(product, HttpStatus.CREATED, EMPTY_MESSAGE);
    }

    @PutMapping("product")
    public CommonResponse<ProductDto> updateProduct(@RequestBody ProductDto productDto) {
        ProductDto product = productService.updateProduct(productDto);
        return CommonResponse.success(product, HttpStatus.OK, EMPTY_MESSAGE);
    }

    @DeleteMapping("/product/{productId}")
    public CommonResponse<Boolean> deleteProductById(@PathVariable Long productId) {
        Boolean result = productService.deleteProductById(productId);
        return CommonResponse.success(result, HttpStatus.OK, EMPTY_MESSAGE);
    }
}
