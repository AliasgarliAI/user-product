package com.company.api.controller;

import com.company.api.dto.CommonResponse;
import com.company.api.exception.CommonNotFoundException;
import com.company.api.exception.InsufficientBalanceException;
import com.company.api.exception.UnAuthorizedException;
import com.company.api.service.inter.ShoppingService;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static com.company.api.constants.ApplicationMessage.OPERATION_PERFORMED;


@RestController
@RequestMapping("/nroduct")
@RequiredArgsConstructor
public class ShoppingController {

    private final ShoppingService shoppingService;

    @PostMapping("/buy")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public CommonResponse<Boolean> buyProduct(
            @RequestHeader("Idempotency-Key")
            @NotBlank(message = "Idempotency-Key must not be blank")
            @Size(min = 5, max = 10, message = "Idempotency-Key length must be between 5 and 10 characters")
            String idempotencyKey,
            @RequestParam("userId") Long userId,
            @RequestParam("productId") Long productId)
            throws InsufficientBalanceException, UnAuthorizedException, CommonNotFoundException {

        Boolean result = shoppingService.buyProduct(idempotencyKey, userId, productId);
        return CommonResponse.success(result,HttpStatus.NO_CONTENT,OPERATION_PERFORMED);
    }

    @PutMapping("/return")
    @ResponseStatus(HttpStatus.OK)
    public CommonResponse<Boolean> returnProduct(
            @RequestHeader("Idempotency-Key")
            @NotBlank(message = "Idempotency-Key must not be blank")
            @Size(min = 5, max = 10, message = "Idempotency-Key length must be between 5 and 10 characters")
            String idempotencyKey,
            @RequestParam("userId") Long userId,
            @RequestParam("productId") Long productId) throws CommonNotFoundException {

        Boolean result = shoppingService.returnProduct(idempotencyKey, userId, productId);
        return CommonResponse.success(result,HttpStatus.NO_CONTENT,OPERATION_PERFORMED);

    }

}
