package com.company.api.controller;

import com.company.api.dto.CommonResponse;
import com.company.api.dto.UserDto;
import com.company.api.exception.CommonNotFoundException;
import com.company.api.service.inter.UserService;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

import static com.company.api.constants.ApplicationMessage.*;

@RestController
@RequestMapping("users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public CommonResponse<List<UserDto>> getAllUsers(@RequestParam(value = "page",required = false) Integer pageNumber,
                                                     @RequestParam(value = "size",required = false) Integer size) {
        List<UserDto> users = userService.getUsers(pageNumber, size);

        return CommonResponse.success(users, HttpStatus.OK, EMPTY_MESSAGE);
    }

    @GetMapping("/active")
    public CommonResponse<List<UserDto>> getAllActiveUsers(
            @RequestParam(value = "page",required = false) Integer pageNumber,
            @RequestParam(value = "size",required = false) Integer size) {
        List<UserDto> activeUsers = userService.getActiveUsers(pageNumber, size);

        return CommonResponse.success(activeUsers, HttpStatus.OK, EMPTY_MESSAGE);
    }

    @GetMapping("/user/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public CommonResponse<UserDto> getUserById(@PathVariable Long userId) throws CommonNotFoundException {
        UserDto user = userService.getUserById(userId);

        return CommonResponse.success(user, HttpStatus.OK, EMPTY_MESSAGE);
    }

    @GetMapping("/user")
    @ResponseStatus(HttpStatus.OK)
    public CommonResponse<UserDto> getUserByUserName(
            @RequestParam("username") String userName) throws CommonNotFoundException {
        UserDto user = userService.getUserByUserName(userName);

        return CommonResponse.success(user, HttpStatus.OK, EMPTY_MESSAGE);
    }

    @GetMapping("/user/balance/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public CommonResponse<BigDecimal> getUserBalanceByUserId(
            @PathVariable Long userId) throws CommonNotFoundException {

        BigDecimal userBalance = userService.getUserBalanceById(userId);

        return CommonResponse.success(userBalance, HttpStatus.OK, EMPTY_MESSAGE);
    }

    @GetMapping("/user/balance/")
    @ResponseStatus(HttpStatus.OK)
    public CommonResponse<BigDecimal> getUserBalanceByUserName(
            @RequestParam("username") String userName) throws CommonNotFoundException {

        BigDecimal userBalance = userService.getUserBalanceByUserName(userName);

        return CommonResponse.success(userBalance, HttpStatus.OK, EMPTY_MESSAGE);
    }

    @PostMapping("/user")
    @ResponseStatus(HttpStatus.CREATED)
    public CommonResponse<UserDto> createNewUser(
            @RequestHeader("Idempotency-Key")
            @NotBlank(message = "Idempotency-Key must not be blank")
            @Size(min = 5, max = 10, message = "Idempotency-Key length must be between 5 and 10 characters")
            String idempotencyKey,
            @RequestBody UserDto userDto) {

        UserDto user = userService.createUser(idempotencyKey, userDto);

        return CommonResponse.success(user, HttpStatus.CREATED, EMPTY_MESSAGE);
    }

    @PutMapping("/user")
    @ResponseStatus(HttpStatus.OK)
    public CommonResponse<UserDto> updateUser(@RequestBody UserDto userDto) {
        UserDto updatedUser = userService.updateUser(userDto);
        return CommonResponse.success(updatedUser, HttpStatus.OK, EMPTY_MESSAGE);
    }

    @DeleteMapping("/user/{userId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public CommonResponse<Boolean> deleteUserById(@PathVariable Long userId){
        boolean isDeleted = userService.deleteUserById(userId);

        return CommonResponse.success(isDeleted,HttpStatus.NO_CONTENT,DELETION_MESSAGE);
    }

    @DeleteMapping("/user/{username}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public CommonResponse<Boolean> deleteUserById(@PathVariable("username") String userName){
        boolean isDeleted = userService.deleteUserByUserName(userName);

        return CommonResponse.success(isDeleted,HttpStatus.NO_CONTENT,DELETION_MESSAGE);
    }

    @PutMapping("/user/enable/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public CommonResponse<Boolean> enableUserByUserId(@PathVariable Long userId) throws CommonNotFoundException {
        boolean enabled = userService.enableUserById(userId);
        return CommonResponse.success(enabled,HttpStatus.OK,ACTIVATION_MESSAGE);
    }

    @PutMapping("/user/enable")
    @ResponseStatus(HttpStatus.OK)
    public CommonResponse<Boolean> enableUserByUserName(
            @RequestParam("username") String userName) throws CommonNotFoundException {
        boolean enabled = userService.enableUserByUserName(userName);
        return CommonResponse.success(enabled,HttpStatus.OK,ACTIVATION_MESSAGE);
    }

    @PutMapping("/user/disable/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public CommonResponse<Boolean> disableUserByUserId(@PathVariable Long userId) throws CommonNotFoundException {
        boolean disabled = userService.disableUserById(userId);
        return CommonResponse.success(disabled,HttpStatus.OK,DEACTIVATION_MESSAGE);
    }
    @PutMapping("/user/disable")
    @ResponseStatus(HttpStatus.OK)
    public CommonResponse<Boolean> disableUserByUserName(
            @RequestParam("username") String userName) throws CommonNotFoundException {
        boolean disabled = userService.disableUserByUserName(userName);
        return CommonResponse.success(disabled,HttpStatus.OK,DEACTIVATION_MESSAGE);
    }
}
