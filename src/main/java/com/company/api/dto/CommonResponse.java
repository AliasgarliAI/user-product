package com.company.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

import java.lang.reflect.InvocationTargetException;

@Slf4j
@Data
@AllArgsConstructor
public class CommonResponse<T> {

    private Boolean success;
    private String message;
    private HttpStatus statusCode;
    private T data;

    private CommonResponse() {
    }

    public static <T> CommonResponse<T> empty(Class<T> classType, boolean success, String message) {
        T emptyData = null;

        try {
            emptyData = (T) classType.getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException |
                 InvocationTargetException e) {
            log.error(e.getMessage());
        }

         return new CommonResponse<T>(success,message,HttpStatus.NO_CONTENT,emptyData);
    }

    public static <T> CommonResponse<T> success(T data, HttpStatus statusCode, String message) {
        return new CommonResponse<T>(true,message,statusCode,data);
    }

    public static <T> CommonResponse<T> error(String message, HttpStatus statusCode) {
        return new CommonResponse<T>(true,message,statusCode,null);
    }
}
