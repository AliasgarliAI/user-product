package com.company.api.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IdempotencyKey {

    private String idempotencyKey;
    private LocalDateTime creationDate;
    private String response;

}
