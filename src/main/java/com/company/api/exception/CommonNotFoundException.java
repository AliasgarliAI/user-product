package com.company.api.exception;

public class CommonNotFoundException extends Exception {
    public CommonNotFoundException(String message) {
        super(message);
    }
}
