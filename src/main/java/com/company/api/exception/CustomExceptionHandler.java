package com.company.api.exception;

import com.company.api.dto.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(CommonNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public CommonResponse<CommonNotFoundException> notFoundException(CommonNotFoundException ex){
        return CommonResponse.error(ex.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UnAuthorizedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public CommonResponse<UnAuthorizedException> unAuthorizedException(UnAuthorizedException ex){
        return CommonResponse.error(ex.getMessage(), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(InsufficientBalanceException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public CommonResponse<InsufficientBalanceException> insufficientBalanceException(InsufficientBalanceException ex){
        return CommonResponse.error(ex.getMessage(), HttpStatus.FORBIDDEN);
    }
}
