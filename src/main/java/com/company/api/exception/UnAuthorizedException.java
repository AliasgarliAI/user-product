package com.company.api.exception;

public class UnAuthorizedException extends Exception{
    public UnAuthorizedException(String message) {
        super(message);
    }
}
