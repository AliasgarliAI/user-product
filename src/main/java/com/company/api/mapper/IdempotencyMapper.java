package com.company.api.mapper;

import com.company.api.entity.IdempotencyKey;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Slf4j
@Component
public class IdempotencyMapper implements RowMapper<IdempotencyKey> {

    @Override
    public IdempotencyKey mapRow(ResultSet rs, int rowNum) throws SQLException {
        IdempotencyKey idempotencyKey = getIdempotencyFromRowContext();

        if (idempotencyKey.getIdempotencyKey() ==null) {
            idempotencyKey.setIdempotencyKey(rs.getString("key"));
            idempotencyKey.setCreationDate(rs.getTimestamp("creation_date").toLocalDateTime());
            idempotencyKey.setResponse(rs.getString("response"));
        }

        idempotencyContext.set(idempotencyKey);
        log.info("this is idempotency ->> {} ", idempotencyKey);
        return idempotencyKey;
    }


    private static final ThreadLocal<IdempotencyKey> idempotencyContext = ThreadLocal.withInitial(IdempotencyKey::new);

    private IdempotencyKey getIdempotencyFromRowContext() {
        IdempotencyKey idempotencyKey = idempotencyContext.get();
        if (idempotencyKey == null) {
            idempotencyKey = new IdempotencyKey();
        }
        return idempotencyKey;
    }
}
