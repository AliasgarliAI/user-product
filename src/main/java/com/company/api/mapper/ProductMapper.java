package com.company.api.mapper;

import com.company.api.dto.ProductDto;
import com.company.api.dto.UserDto;
import com.company.api.entity.Product;
import com.company.api.entity.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    ProductDto productToDto(Product product);
    Product dtoToProduct(ProductDto product);

    List<ProductDto> productsToDtoList(List<Product> productList);
    List<Product> dtoListToProducts(List<ProductDto> productDtoList);

}
