package com.company.api.mapper;

import com.company.api.dto.UserDto;
import com.company.api.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mapping(target = "isActive",source = "active")
    UserDto userToDto(User user);
    User dtoToUsers(UserDto userDto);

    List<UserDto> userListToDtoList(List<User> userList);
    List<User> dtoListToUserList(List<UserDto> userDtoList);

}
