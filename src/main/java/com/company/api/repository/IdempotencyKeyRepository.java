package com.company.api.repository;

import com.company.api.entity.IdempotencyKey;
import com.company.api.mapper.IdempotencyMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class IdempotencyKeyRepository {

    private final JdbcTemplate jdbcTemplate;
    private final IdempotencyMapper idempotencyMapper;

    public Optional<IdempotencyKey> getIdempotencyKey(String key) {

        String query = "select * from \"idempotency_key\" u where u.key = ?";

        return jdbcTemplate.query(query, idempotencyMapper, key).stream().findFirst();

    }

    public IdempotencyKey saveKey(String newKey, String response){

        IdempotencyKey key = new IdempotencyKey();
        key.setIdempotencyKey(newKey);
        key.setCreationDate(LocalDateTime.now());
        key.setResponse(response);

        String query = "INSERT INTO idempotency_key (key, creation_date, response) VALUES (?, ?, ?)";
        jdbcTemplate.update(query, key.getIdempotencyKey(), key.getCreationDate(), key.getResponse());

        return key;

    }

}
