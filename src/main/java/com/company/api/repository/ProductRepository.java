package com.company.api.repository;

import com.company.api.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product,Long> {

    @Query(value = "select p from products p where p.stock_count > 0",nativeQuery = true)
    List<Product> findExistingProducts();

    List<Product> findProductsByProductName(String productName);

}
