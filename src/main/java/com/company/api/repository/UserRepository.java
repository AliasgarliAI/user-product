package com.company.api.repository;

import com.company.api.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Long> {

    @Query(value = "select u from users u where u.is_active = true", nativeQuery = true)
    List<User> getUserByActiveIsTrue();

    @Query(value = "select u.balance from users u where u.id=:id",nativeQuery = true)
    Optional<BigDecimal> getUserBalanceById(Long id);

    @Query(value = "select u.balance from users u where u.username  = :userName",nativeQuery = true)
    Optional<BigDecimal> getUserBalanceByUserName(String userName);
    Optional<User> findByUserName(String userName);

    void deleteUserByUserName(String userName);

}
