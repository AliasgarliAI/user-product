package com.company.api.service.impl;

import com.company.api.entity.IdempotencyKey;
import com.company.api.repository.IdempotencyKeyRepository;
import com.company.api.service.inter.IdempotencyService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class IdempotencyServiceImpl implements IdempotencyService {

    private final IdempotencyKeyRepository idempotencyKeyRepository;
    @Override
    public boolean getIdempotencyByKey(String idempotencyKey) {
        return idempotencyKeyRepository.getIdempotencyKey(idempotencyKey).isPresent();
    }

    @Override
    public String saveIdempotency(String key,String response) {
        return idempotencyKeyRepository.saveKey(key,response).getIdempotencyKey();
    }
}
