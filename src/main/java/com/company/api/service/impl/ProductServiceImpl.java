package com.company.api.service.impl;

import com.company.api.dto.ProductDto;
import com.company.api.entity.Product;
import com.company.api.exception.CommonNotFoundException;
import com.company.api.mapper.ProductMapper;
import com.company.api.repository.ProductRepository;
import com.company.api.service.inter.IdempotencyService;
import com.company.api.service.inter.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.company.api.constants.ExceptionMessage.*;

@Service
@Transactional
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductMapper productMapper;
    private final ProductRepository productRepository;
    private final IdempotencyService idempotencyService;

    @Override
    public List<ProductDto> getAllProducts(Integer pageNumber, Integer size) {
        List<Product> products = productRepository.findAll(
                (pageNumber == null || size == null) ?
                        Pageable.unpaged() : PageRequest.of(pageNumber, size)).getContent();
        return productMapper.productsToDtoList(products);
    }

    @Override
    public List<ProductDto> getAllExistingProducts() {
        List<Product> existingProducts = productRepository.findExistingProducts();
        return productMapper.productsToDtoList(existingProducts);
    }

    @Override
    public ProductDto getProductById(Long id) throws CommonNotFoundException {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new CommonNotFoundException(PRODUCT_NOT_FOUND));
        return productMapper.productToDto(product);
    }

    @Override
    public List<ProductDto> getProductByName(String productName) throws CommonNotFoundException {
        List<Product> products = productRepository.findProductsByProductName(productName);
        if (products.isEmpty())
            throw new CommonNotFoundException(PRODUCT_NOT_FOUND);
        return productMapper.productsToDtoList(products);
    }

    @Override
    public ProductDto createProduct(String idempotencyKey,ProductDto productDto) {

        if (!idempotencyService.getIdempotencyByKey(idempotencyKey)){
            Product product = productRepository.save(productMapper.dtoToProduct(productDto));
            idempotencyService.saveIdempotency(idempotencyKey,
                    String.format("New product object is created with id %d",product.getId()));
            productDto = productMapper.productToDto(product);
        }

        return productDto;
    }

    @Override
    public ProductDto updateProduct(ProductDto productDto) {
        Product product = productRepository.save(productMapper.dtoToProduct(productDto));
        return productMapper.productToDto(product);
    }

    @Override
    public boolean deleteProductById(Long productId) {
        productRepository.deleteById(productId);
        return true;
    }


}
