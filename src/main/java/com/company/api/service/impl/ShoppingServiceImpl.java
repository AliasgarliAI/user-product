package com.company.api.service.impl;

import com.company.api.dto.ProductDto;
import com.company.api.dto.UserDto;
import com.company.api.exception.CommonNotFoundException;
import com.company.api.exception.InsufficientBalanceException;
import com.company.api.exception.UnAuthorizedException;
import com.company.api.service.inter.IdempotencyService;
import com.company.api.service.inter.ShoppingService;
import com.company.api.service.inter.ProductService;
import com.company.api.service.inter.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.company.api.constants.ExceptionMessage.*;

@Service
@Transactional
@RequiredArgsConstructor
public class ShoppingServiceImpl implements ShoppingService {

    private final UserService userService;
    private final ProductService productService;
    private final IdempotencyService idempotencyService;

    @Override
    public Boolean buyProduct(String idempotencyKey, Long userId, Long productId)
            throws InsufficientBalanceException, UnAuthorizedException, CommonNotFoundException {
        if (!idempotencyService.getIdempotencyByKey(idempotencyKey)) {

            UserDto user = userService.getUserById(userId);

            if (!user.isActive())
                throw new UnAuthorizedException(OPERATION_NOT_ACCEPTED);

            ProductDto productDto = productService.getProductById(productId);

            if (productDto.getPrice().compareTo(user.getBalance()) > 0)
                throw new InsufficientBalanceException(INSUFFICIENT_BALANCE);

            user.setBalance(user.getBalance().subtract(productDto.getPrice()));
            productDto.setStockCount(productDto.getStockCount() - 1);

            userService.updateUser(user);
            productService.updateProduct(productDto);
        }

        return true;
    }

    @Override
    public Boolean returnProduct(String idempotencyKey, Long userId, Long productId) throws CommonNotFoundException {
        if (!idempotencyService.getIdempotencyByKey(idempotencyKey)) {
            UserDto user = userService.getUserById(userId);
            ProductDto product = productService.getProductById(productId);

            user.setBalance(user.getBalance().add(product.getPrice()));
            product.setStockCount(product.getStockCount() + 1);
            userService.updateUser(user);
            productService.updateProduct(product);
        }
        return true;
    }
}
