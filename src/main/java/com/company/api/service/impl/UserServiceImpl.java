package com.company.api.service.impl;

import com.company.api.dto.UserDto;
import com.company.api.entity.User;
import com.company.api.exception.CommonNotFoundException;
import com.company.api.mapper.UserMapper;
import com.company.api.repository.UserRepository;
import com.company.api.service.inter.IdempotencyService;
import com.company.api.service.inter.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

import static com.company.api.constants.ExceptionMessage.USER_NOT_FOUND;

@Service
@Transactional
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final IdempotencyService idempotencyService;
    private final UserMapper userMapper;


    public List<UserDto> getUsers(Integer pageNumber, Integer size) {

        return userMapper.userListToDtoList(
                userRepository.findAll(
                                (pageNumber == null || size == null) ?
                                        Pageable.unpaged() : PageRequest.of(pageNumber, size))
                        .getContent());

    }

    public List<UserDto> getActiveUsers(Integer pageNumber, Integer size) {
        return userMapper.userListToDtoList(userRepository.getUserByActiveIsTrue());
    }

    @Override
    public UserDto getUserById(Long userId) throws CommonNotFoundException {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new CommonNotFoundException(USER_NOT_FOUND));
        return userMapper.userToDto(user);
    }

    @Override
    public UserDto getUserByUserName(String userName) throws CommonNotFoundException {
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new CommonNotFoundException(USER_NOT_FOUND));
        return userMapper.userToDto(user);
    }

    @Override
    public BigDecimal getUserBalanceById(Long userId) throws CommonNotFoundException {
        return userRepository.getUserBalanceById(userId)
                .orElseThrow(()->new CommonNotFoundException(USER_NOT_FOUND));
    }

    @Override
    public BigDecimal getUserBalanceByUserName(String userName) throws CommonNotFoundException {
        return userRepository.getUserBalanceByUserName(userName)
                .orElseThrow(()->new CommonNotFoundException(USER_NOT_FOUND));
    }

    @Override
    public UserDto updateUser(UserDto userDto) {
        User user = userRepository.saveAndFlush(userMapper.dtoToUsers(userDto));
        return userMapper.userToDto(user);
    }

    @Override
    public UserDto createUser(String idempotencyKey,UserDto userDto) {

        if (!idempotencyService.getIdempotencyByKey(idempotencyKey)){
            User newUser = userRepository.save(userMapper.dtoToUsers(userDto));
            idempotencyService.saveIdempotency(idempotencyKey,
                    String.format("New user object is created with id %s",newUser.getId()));
            userDto = userMapper.userToDto(newUser);
        }

        return userDto;
    }

    @Override
    public boolean deleteUserById(Long userId) {
        userRepository.deleteById(userId);
        return true;
    }

    @Override
    public boolean deleteUserByUserName(String userName) {
        userRepository.deleteUserByUserName(userName);
        return true;
    }

    @Override
    public boolean enableUserById(Long userId) throws CommonNotFoundException {
        User user = userRepository.findById(userId)
                .orElseThrow(()-> new CommonNotFoundException(USER_NOT_FOUND));
        user.setActive(true);
        return true;
    }

    @Override
    public boolean disableUserById(Long userId) throws CommonNotFoundException {
        User user = userRepository.findById(userId)
                .orElseThrow(()-> new CommonNotFoundException(USER_NOT_FOUND));
        user.setActive(false);
        return true;
    }

    @Override
    public boolean enableUserByUserName(String userName) throws CommonNotFoundException {
        User user = userRepository.findByUserName(userName)
                .orElseThrow(()-> new CommonNotFoundException(USER_NOT_FOUND));
        user.setActive(true);
        return true;
    }

    @Override
    public boolean disableUserByUserName(String userName) throws CommonNotFoundException {
        User user = userRepository.findByUserName(userName)
                .orElseThrow(()-> new CommonNotFoundException(USER_NOT_FOUND));
        user.setActive(false);
        return true;
    }

    @Override
    public boolean isUserActive(Long userId) throws CommonNotFoundException {
        User user = userRepository.findById(userId)
                .orElseThrow(()-> new CommonNotFoundException(USER_NOT_FOUND));
        return user.isActive();
    }

}
