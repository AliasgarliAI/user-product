package com.company.api.service.inter;

public interface IdempotencyService {

    boolean getIdempotencyByKey(String idempotencyKey);
    String saveIdempotency(String key,String response);
}
