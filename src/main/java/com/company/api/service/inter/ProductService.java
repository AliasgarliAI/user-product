package com.company.api.service.inter;

import com.company.api.dto.ProductDto;
import com.company.api.exception.CommonNotFoundException;

import java.util.List;

public interface ProductService {

    List<ProductDto> getAllProducts(Integer pageNumber, Integer size);

    List<ProductDto> getAllExistingProducts();

    ProductDto getProductById(Long id) throws CommonNotFoundException;

    List<ProductDto> getProductByName(String productName) throws CommonNotFoundException;

    ProductDto createProduct(String idempotencyKey,ProductDto productDto);

    ProductDto updateProduct(ProductDto productDto);

    boolean deleteProductById(Long productId);


}
