package com.company.api.service.inter;

import com.company.api.exception.CommonNotFoundException;
import com.company.api.exception.InsufficientBalanceException;
import com.company.api.exception.UnAuthorizedException;

public interface ShoppingService {

    Boolean buyProduct(String idempotencyKey ,Long userId,Long productId) throws InsufficientBalanceException, UnAuthorizedException, CommonNotFoundException;
    Boolean returnProduct(String idempotencyKey,Long userId,Long productId) throws CommonNotFoundException;
}
