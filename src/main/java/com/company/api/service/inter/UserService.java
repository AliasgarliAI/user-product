package com.company.api.service.inter;

import com.company.api.dto.UserDto;
import com.company.api.exception.CommonNotFoundException;

import java.math.BigDecimal;
import java.util.List;

public interface UserService {

    List<UserDto> getUsers(Integer pageNumber, Integer size);

    List<UserDto> getActiveUsers(Integer pageNumber, Integer size);

    UserDto getUserById(Long userId) throws CommonNotFoundException;

    UserDto getUserByUserName(String userName) throws CommonNotFoundException;

    BigDecimal getUserBalanceById(Long userId) throws CommonNotFoundException;

    BigDecimal getUserBalanceByUserName(String userName) throws CommonNotFoundException;

    UserDto updateUser(UserDto userDto);

    UserDto createUser(String idempotencyKey,UserDto userDto);

    boolean deleteUserById(Long userId);

    boolean deleteUserByUserName(String userName);

    boolean enableUserById(Long userId) throws CommonNotFoundException;

    boolean disableUserById(Long userId) throws CommonNotFoundException;

    boolean enableUserByUserName(String userName) throws CommonNotFoundException;

    boolean disableUserByUserName(String userName) throws CommonNotFoundException;

    boolean isUserActive(Long userId) throws CommonNotFoundException;

}
